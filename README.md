[A little something for ETHOnline](https://showcase.ethglobal.com/ethonline2021/sticker-shock).

The entire system is [self-contained in index.html](https://goji.link/ethonline/) - do consider dropping in your own [Covalent API key](https://www.covalenthq.com/platform/) instead of mine if you're using it a bunch. ;)
